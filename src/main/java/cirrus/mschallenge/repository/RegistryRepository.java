package cirrus.mschallenge.repository;

import cirrus.mschallenge.entity.ChRegistry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface RegistryRepository extends JpaRepository<ChRegistry, Integer> {

    @Query("SELECT it FROM ChRegistry it ORDER BY it.registryId DESC")
    List<ChRegistry> findRegistryList();
}
