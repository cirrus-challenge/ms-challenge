package cirrus.mschallenge.bl;

import cirrus.mschallenge.dto.RegistryRequestDto;
import cirrus.mschallenge.dto.RegistryResponseDto;
import cirrus.mschallenge.entity.ChRegistry;
import cirrus.mschallenge.repository.RegistryRepository;
import cirrus.mschallenge.util.DateUtil;
import net.cirrus.it.commons.microservices.exception.ServiceException;
import net.cirrus.it.commons.microservices.util.Invoke;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RegistryBl {

    private static Logger LOGGER = LoggerFactory.getLogger(RegistryBl.class);
    private DateUtil dateUtil;
    private RegistryRepository registryRepository;


    @Autowired
    public RegistryBl(DateUtil dateUtil,RegistryRepository registryRepository) {
        this.dateUtil = dateUtil;
        this.registryRepository = registryRepository;


    }

    @Transactional
    public List<RegistryResponseDto> getRegistryList() {
        LOGGER.info("QUERY-DB: Consulta de registros");
        List<ChRegistry> data = Invoke.andFail(() -> registryRepository.findRegistryList(),
                ex -> LOGGER.error("ERROR-DB: Error en la consulta", ex));
        LOGGER.info("SUCCESS-DB: Consulta exitosa de registros");
        Stream<RegistryResponseDto> dtoStream = data.stream().map( it ->
                new RegistryResponseDto(
                        it.getRegistryId(),
                        it.getRegistryCode(),
                        it.getRegistryName(),
                        dateUtil.formatDateToString(it.getRegistryDate())
                )
        );
        return dtoStream.collect(Collectors.toList());
    }

    @Transactional
    public String saveRegistry( RegistryRequestDto dto) {
        ChRegistry entity = new ChRegistry();
        Date registryDate = dateUtil.validateGreaterDate(dto.getRegistryDate());
        entity.setRegistryCode(dto.getRegistryCode());
        entity.setRegistryName(dto.getRegistryName());
        entity.setRegistryDate(registryDate);
        LOGGER.info("BL-DB: Registro en la tabla registry");
        Invoke.andFail(
                () -> registryRepository.saveAndFlush(entity),
                ex -> {
                    LOGGER.error("ERROR-DB: Error en el registro", ex);
                    throw new ServiceException(1, "El código de registro ya existe");
                });
        LOGGER.info("BL-SUCCESS: Registro fue exitoso");
        return "Registro exitoso";
    }



}
