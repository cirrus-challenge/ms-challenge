package cirrus.mschallenge.util;

import net.cirrus.it.commons.microservices.exception.ServiceException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Service
public class DateUtil {

    public String formatDateToString (Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("America/La_Paz"));
        return dateFormat.format(date);
    }

    public Date parseStringToDate(String stringDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("America/La_Paz"));
        Date result = new Date();
        try{
            result = dateFormat.parse(stringDate);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return result;
    }

    public String formatTimeDateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("America/La_Paz"));
        return dateFormat.format(date).substring(0,10);
    }

    public Date formatCurrentDate(Date date){
        String currentDate  = formatDateToString(date);
        return parseStringToDate(currentDate);
    }

    public Date validateGreaterDate (String entryDate) {
        Date date = parseStringToDate(entryDate);
        Date currentDate = formatCurrentDate(new Date());
        if (currentDate.before(date)) {
            throw new ServiceException(1,"La fecha ingresada es mayor a la fecha actual");
        }
        return  date;
    }




}
