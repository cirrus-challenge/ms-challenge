package cirrus.mschallenge.dto;

public class RegistryResponseDto {
    private int registryId;
    private String registryCode;
    private String registryName;
    private String registryDate;

    public RegistryResponseDto() {
    }


    public RegistryResponseDto(int registryId, String registryCode, String registryName, String registryDate) {
        this.registryId = registryId;
        this.registryCode = registryCode;
        this.registryName = registryName;
        this.registryDate = registryDate;
    }

    public int getRegistryId() {
        return registryId;
    }

    public void setRegistryId(int registryId) {
        this.registryId = registryId;
    }

    public String getRegistryCode() {
        return registryCode;
    }

    public void setRegistryCode(String registryCode) {
        this.registryCode = registryCode;
    }

    public String getRegistryName() {
        return registryName;
    }

    public void setRegistryName(String registryName) {
        this.registryName = registryName;
    }

    public String getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(String registryDate) {
        this.registryDate = registryDate;
    }
}
