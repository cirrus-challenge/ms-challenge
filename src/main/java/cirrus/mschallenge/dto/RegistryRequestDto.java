package cirrus.mschallenge.dto;

public class RegistryRequestDto {
    private String registryCode;
    private String registryName;
    private String registryDate;

    public RegistryRequestDto() {
    }


    public RegistryRequestDto(String registryCode, String registryName, String registryDate) {
        this.registryCode = registryCode;
        this.registryName = registryName;
        this.registryDate = registryDate;
    }

    public String getRegistryCode() {
        return registryCode;
    }

    public void setRegistryCode(String registryCode) {
        this.registryCode = registryCode;
    }

    public String getRegistryName() {
        return registryName;
    }

    public void setRegistryName(String registryName) {
        this.registryName = registryName;
    }

    public String getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(String registryDate) {
        this.registryDate = registryDate;
    }
}
