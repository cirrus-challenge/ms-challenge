/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cirrus.mschallenge.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author sergio
 */
@Entity
@Table(name = "ch_registry")
@NamedQueries({
    @NamedQuery(name = "ChRegistry.findAll", query = "SELECT c FROM ChRegistry c")})
public class ChRegistry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chRegistry")
    @SequenceGenerator(name="chRegistry", sequenceName = "sq_registry_id", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "registry_id")
    private Integer registryId;
    @Basic(optional = false)
    @Column(name = "registry_code")
    private String registryCode;
    @Basic(optional = false)
    @Column(name = "registry_name")
    private String registryName;
    @Basic(optional = false)
    @Column(name = "registry_date")
    @Temporal(TemporalType.DATE)
    private Date registryDate;

    public ChRegistry() {
    }

    public ChRegistry(Integer registryId) {
        this.registryId = registryId;
    }

    public ChRegistry(Integer registryId, String registryCode, String registryName, Date registryDate) {
        this.registryId = registryId;
        this.registryCode = registryCode;
        this.registryName = registryName;
        this.registryDate = registryDate;
    }

    public Integer getRegistryId() {
        return registryId;
    }

    public void setRegistryId(Integer registryId) {
        this.registryId = registryId;
    }

    public String getRegistryCode() {
        return registryCode;
    }

    public void setRegistryCode(String registryCode) {
        this.registryCode = registryCode;
    }

    public String getRegistryName() {
        return registryName;
    }

    public void setRegistryName(String registryName) {
        this.registryName = registryName;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registryId != null ? registryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChRegistry)) {
            return false;
        }
        ChRegistry other = (ChRegistry) object;
        if ((this.registryId == null && other.registryId != null) || (this.registryId != null && !this.registryId.equals(other.registryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "net.cirrus.it.collections.mscollections.dao.domain.ChRegistry[ registryId=" + registryId + " ]";
    }
    
}
