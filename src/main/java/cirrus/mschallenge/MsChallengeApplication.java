package cirrus.mschallenge;

import net.cirrus.it.commons.microservices.dto.ResponseDto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsChallengeApplication {

	public static void main(String[] args) {
		ResponseDto.setService("MSCH");
		SpringApplication.run(MsChallengeApplication.class, args);
	}

}
