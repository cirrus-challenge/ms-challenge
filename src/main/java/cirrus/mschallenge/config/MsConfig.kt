package cirrus.mschallenge.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("net.cirrus.it.commons.microservices.component")
open class MsConfig
