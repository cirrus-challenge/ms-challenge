package cirrus.mschallenge.api;


import cirrus.mschallenge.bl.RegistryBl;
import cirrus.mschallenge.dto.RegistryRequestDto;
import cirrus.mschallenge.dto.RegistryResponseDto;
import net.cirrus.it.commons.microservices.dto.ResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/registry")
public class RegistryController {
    private static Logger LOGGER = LoggerFactory.getLogger(RegistryController.class);
    private RegistryBl registryBl;

    @Autowired
    public RegistryController(RegistryBl registryBl) {
        this.registryBl = registryBl;
    }



    @RequestMapping(
            value = "/list",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<List<RegistryResponseDto>>> getRegistryList() {
        LOGGER.info("REQUEST: Solicitud para obtener listado de registros");
        List<RegistryResponseDto> data = registryBl.getRegistryList();
        LOGGER.info("SUCCESS-REQUEST: Solicitud para obtener registros fue exitosa");
        return new ResponseEntity<>(ResponseDto.newInstance(data), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/error",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<String>> showRegistryErrorNotification() {
        LOGGER.error("NOTIFICATION: Error en registro en la tabla ChRegistry");
        return new ResponseEntity<>(ResponseDto.newInstance("Error en registro en la tabla ChRegistry"), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/success",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<String>> showRegistrySuccessNotification() {
        LOGGER.info("NOTIFICATION: Registro exitoso en la tabla ChRegistry");
        return new ResponseEntity<>(ResponseDto.newInstance("Registro exitoso en la tabla ChRegistry"), HttpStatus.OK);
    }


    @RequestMapping(
            value = "/save",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<String>> saveRegistry(@Validated @RequestBody RegistryRequestDto dto) {
        LOGGER.info("REQUEST: Solicitud para realizar un registro");
        String data = registryBl.saveRegistry(dto);
        LOGGER.info("SUCCESS-REQUEST: Solicitud exitosa de registro");
        return new ResponseEntity<>(ResponseDto.newInstance(data), HttpStatus.OK);
    }



}
